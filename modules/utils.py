#!/usr/bin/python2
# encoding: utf-8

def format_timestring(timelist):
	timeint = 0
	for i in timelist: #convert given time to float
		try:
			num=int(i[:-1]) #the number part
			if i[-1]=='d':
				timeint+=num*3600*24
			elif i[-1]=='h':
				timeint+=num*3600
			elif i[-1]=='m':
				timeint+=num*60
			elif i[-1]=='s':
				timeint+=num
			else:
				raise Exception("doh!")
		except:
			raise Exception("Bad argument @ \"{0}\", format is: 1h 2m 3s".format(i))
	return timeint

def split_large_message(text, delimiter="\n", charlimit=1999):
	templist = text.split(delimiter)
	tempstr = ""
	result = []
	print(templist)
	for i in range(0,len(templist)):
		tempstr += templist[i]
		msglen = len(tempstr)
		if msglen >= charlimit:
			raise("Text split failed!")
		elif i+1 < len(templist):
			tempstr += delimiter
			if msglen+len(templist[i+1]) >= charlimit-2:
				result.append(tempstr)
				tempstr = ""
		else:
			result.append(tempstr)
	return result

ranks = {
	5000: "⟦★⟧",
	4500: "⟦Ⅹ⟧",
	4000: "⟦Ⅸ⟧",
	3500: "⟦Ⅷ⟧",
	3000: "⟦Ⅶ⟧",
	2500: "⟦Ⅵ⟧",
	2000: "⟦Ⅴ⟧",
	1500: "⟦Ⅳ⟧",
	1000: "⟦Ⅲ⟧",
	500: "⟦Ⅱ⟧",
	0: "⟦Ⅰ⟧"}

season_ranks = {
	2146: " ⟨S⟩ ",
	1195: " ⟨A⟩ ",
	751: " ⟨B⟩ ",
	210: " ⟨C⟩ ",
	-79: " ⟨D⟩ ",
	-465: " ⟨E⟩ ",
	-1020: " ⟨F⟩ "}

percent_ranks = {
	4: " ⟨S⟩ ", #    4%
	14: " ⟨A⟩ ", #  10%
	30: " ⟨B⟩ ", #  16%
	52: " ⟨C⟩ ", #  22%
	74: " ⟨D⟩ ", #  22%
	90: " ⟨E⟩ ", #  16%
	100: " ⟨F⟩ "} # 10%

def rating_to_icon(rating, sigma):
	if sigma >= 4.9:
		return("⟦？⟧")
	for i in sorted(ranks.keys(), reverse=True):
		if rating >= i:
			return(ranks[i])
	return("⟦　⟧")

def season_rating_to_icon(rating):
	for i in sorted(season_ranks.keys(), reverse=True):
		if rating >= i:
			return(season_ranks[i])
	return(" ⟨-⟩ ")

# def season_rating_to_icon(user_rank, total_players_number):
# 	for i in sorted(percent_ranks.keys(), reverse=False):
# 		if user_rank/total_players_number*100 <= i:
# 			return(percent_ranks[i])

def channel_to_region(channel_id):
    if channel_id == 781876925978247188:
        return "UNRANK"
    elif channel_id == 738634523839037460:
        return "RANKED"
    elif channel_id == 728103331670655057:
        return "OLD"
    elif channel_id == 790839602075140126:
        return "TEST"
    else:
        return "???"