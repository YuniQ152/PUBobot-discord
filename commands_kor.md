## 사용 가능 명령어

`!enable_pickups`을 제외한 모든 명령어는 채널에 픽업을 활성화해야 작동합니다. 픽업을 활성화하기 위해서는 해당 채널에 `!enable_pickups`를 입력하세요.

Syntax: `()` 필수, `[]` 선택사항

---

### 행동

`!add (pickup)[ pickup ..]` 또는 `+(pickup)[ pickup ..]`: 특정한 픽업 대기열에 참가합니다.

`!add` or `++`: 활동중인 모든 픽업 대기열에 참가합니다.

`!remove (pickup)[ pickup ...]` 또는 `-(pickup)[ pickup ..]`: 특정한 픽업 대기열에서 빠집니다.

`!remove` or `--`: 모든 픽업 대기열에서 빠집니다.

`!expire (시간)`: (시간) 이후 모든 픽업 대기열에서 자동으로 빠집니다. 예시: '!expire 1h 2m 3s'.

`!default_expire (시간 또는 'afk' 또는 none)`: Set your personal default !expire time or set autoremove on afk status.

`!sub`: 진행중인 게임에 대체 플레이어를 요청합니다.

`!subfor (@멘션)`: (@멘션)된 플레이어의 대체 플레이어가 됩니다.

`!allowoffline` 또는 `!ao`: 픽업이 시작될 때까지 오프라인 또는 자리 비움 상태로 대기열에서 빠지지 않도록 합니다.

`!subscribe pickup[ pickup ..]`: 지정된 프로모션 역할(들)을 유저에게 지급합니다.

`!unsubscribe pickup[ pickup ..]`: 지정된 프로모션 역할(들)을 유저에게 제거합니다.


---


### 정보

`!who`: 픽업 대기열에 있는 플레이어의 목록입니다.

`!pickups`: 이 채널의 모든 픽업 대기열 목록입니다.

`!pickup_groups`: 이 채널에서 설정된 모든 픽업 대기열 그룹 목록입니다.

`!expire`: 모든 픽업 대기열에서 자동으로 빠질 때 까지의 남은 시간입니다.

`!lastgame [@멘션 또는 pickup]`: 마지막 픽업 경기 또는 마지막 지정된 마지막 경기입니다.

`!top [pickup] ['weekly' 또는 'monthly' 또는 'yearly']`: 가장 활동적인 플레이어압니다.

`!stats [@멘션 또는 pickup]`: shows you overall stats or stats for specified argument.   

`!ip [pickup 또는 'default']`: shows you ip of last pickup or specified pickup.   

`!maps [pickup]`: 특정한 픽업의 맵 목록입니다.


---


### 팀 선택

*(이 명령어들을 사용하기 위해서는 픽업 설정에 팀이 활성화되어 있어야 합니다.)*

`!pick (@멘션)`: (@멘션)된 플레이어를 자신의 팀으로 선택합니다. *(팀의 캡틴만 사용할 수 있습니다.)* *(팀 선택 방식이 수동일 때만 사용할 수 있습니다.)*

`!teams`: 현재 참여중인 경기의 팀입니다.


---


### 랭킹

*(이 명령어들은 모두 ranked가 활성화 되어있을때만 사용할 수 있습니다.)*

`!leaderboard [페이지]]` 또는 `!lb [페이지]`: 시즌 ELO의 순위입니다.

`!atlb [페이지]`: 전체기간 ELO의 순위입니다.

`!rank [닉네임]`: [닉네임]의 기록된 시즌 ELO, 승률 및 최근 게임 등의 정보입니다.

`!atrank [닉네임]`: [닉네임]의 기록된 전체기간 ELO, 승률 및 최근 게임 등의 정보입니다.

`!reportlose` 또는 `!rl`: 게임 결과를 본인의 팀이 패배한 것으로 보고합니다. *(팀의 캡틴만 사용할 수 있습니다.)*

`!reportdraw` 또는 `!rd`: 게임 결과를 무승부로 보고합니다. 이 명령어는 양 팀의 캡틴이 입력해야 게임 결과가 보고됩니다. *(팀의 캡틴만 사용할 수 있습니다.)*

`!rc`: 픽업 경기가 취소된 것으로 보고합니다. 이 명령어는 양 팀의 캡틴이 입력해야 게임 결과가 보고됩니다. *(팀의 캡틴만 사용할 수 있습니다.)*

`!reportwin (경기 ID) ('alpha' 또는 'beta')` 또는 `!rw (경기 ID) ('alpha' 또는 'beta')`: (경기 ID)를 ('alpha' 또는 'beta')팀이 승리한 것으로 보고합니다. *(모더레이터 또는 그 상위 권한만 사용할 수 있습니다.)*

`!matches`: 진행중인 픽업 경기 목록입니다.

`!ranks_table`: 랭크 표입니다.

`!undo_ranks (경기 ID)`: (경기 ID)로 인해 변경된 ELO를 되돌립니다. *(모더레이터 또는 그 상위 권한만 사용할 수 있습니다.)*


---


### 픽업 관리하기

*(`!noadds`를 제외한 명령어들은 모두 모더레이터 또는 그 상위 권한만 사용할 수 있습니다.)*

`!noadd (@멘션) [기간] [사유]`: (@멘션)된 사람을 [기간]동안 [사유]로 픽업에 참여를 금지시킵니다. [기간]이 없을 경우 픽업 참여 금지 기간은 default_bantime의 값이 됩니다.

`!noadds`: 픽업에 참여 금지된 플레이어 목록입니다.

`!forgive (@멘션)`: (@멘션)된 사람의 픽업 참여 금지를 해제시킵니다.

`!phrase (@멘션) (텍스트)`: (@멘션)된 사람이 픽업 대기열에 참여할 때 (텍스트)를 출력합니다.

`!remove_player (@멘션)`: (@멘션)된 플레이어를 모든 픽업 대기열에서 빠지게 합니다.

`!reset`: 모든 플레이어를 모든 픽업 대기열에서 빠지게 합니다.

`!start (픽업)`: (픽업) 픽업을 강제로 시작합니다.

`!cancel_match (경기 ID)`: (경기 ID)번째 경기를 취소시킵니다. (경기 ID)는 픽업이 시작될 때 나오는 번호입니다. *(ranked가 활성화 되어있을때만 사용할 수 있습니다.)*

`!reset_season`: 시즌 ELO를 초기화합니다. <b>경고: 이 동작은 되돌릴 수 없습니다!</b> *(ranked가 활성화 되어있을때만 사용할 수 있습니다.)*


---


### 픽업 설정하기

*(이 명령어들은 모두 모더레이터 또는 그 상위 권한만 사용할 수 있습니다.)*

`!enable_pickups`: 해당 채널의 픽업을 활성화합니다.

`!disable_pickups`: 해당 채널의 픽업을 비활성화합니다.

`!add_pickups (픽업:플레이어수)[ 픽업:플레이어수 ...]`: 픽업을 (픽업) 이름으로, 최대 인원이 (플레이어수)만큼으로 생성합니다.

`!remove_pickups (픽업)[ 픽업 ...]`: (픽업) 대기열을 삭제합니다.

`!add_pickup_group (그룹) (픽업)[ 픽업...]`: 그룹을 (그룹) 이름으로 (픽업)[ 픽업...]을 포함하여 생성합니다.

`!remove_pickup_group (그룹)`: (그룹)을 삭제합니다.

`!reset_stats`: delete all channel statistics.   

`!cfg`: 해당 채널의 기본 설정입니다.

`!pickup_cfg (픽업)`: (픽업)의 설정입니다.

`!set_ao_for_all <i>name</i> 0|1`: allow/disallow offline for all users of specific pickup kind.


`!set_default (변수명) (값)`: 해당 채널에서 기본 설정의 (변수명)을 (값)으로 설정합니다.
사용 가능 변수: admin_role, moderator_role, captains_role, prefix, default_bantime, ++_req_players, startmsg, submsg, ip, password, maps, pick_captains, pick_teams, promotion_role, promotion_delay, blacklist_role, whitelist_role, require_ready, ranked, ranked_calibrate, ranked_multiplayer, help_answer.   


`!set_pickups (픽업)[ 픽업...] (변수명) (값)`: 특정한 (픽업)[ 픽업...]의 설정에서 (변수명)을 (값)으로 설정합니다.
사용 가능 변수: maxplayers, startmsg, submsg, ip, password, maps, pick_captains, pick_teams, pick_order, promotion_role, blacklist_role, whitelist_role, captains_role, require_ready, ranked, help_answer.   



##### 설정 가능 변수

*모든 변수는 값을 'none'으로 함으로서 변수를 비활성화 할 수 있습니다.*

형식: ``변수명 (값)``

* `admin_role (역할이름)` - (역할이름)을 가지고 있는 플레이어가 픽업 설정/관리 명령어를 사용할 수 있게 합니다. (역할이름)은 역할 멘션이 아닌 역할 이름만을 입력해야 합니다. 관리자 권한이 있는 플레이어는 (역할이름)을 가지고 있지 않아도 픽업 설정/관리 명령어를 사용할 수 있습니다.

* `moderator_role (역할이름)` - (역할이름)을 가지고 있는 플레이어가 픽업 관리 명령어를 사용할 수 있게 합니다. (역할이름)은 역할 멘션이 아닌 역할 이름만을 입력해야 합니다.

* `captains_role (역할이름)` - random captains will be preffered to this role.

* `prefix (symbol)` - 모든 명령어의 Prefix(명령어 접두사)를 변경합니다. 기본값은 '!'입니다.

* `default_bantime (기간)` - !noadd 명령어의 기본 픽업 참여 금지 기간입니다.

* `++_req_players (숫자)` - set minimum pickup required players amount for '++' command or '!add' command without argument, so users wont add to 1v1/2v2 pickups unintentionally. 기본값은 5입니다.

* `startmsg (텍스트)` - set message on a pickup start. Use %ip% and %password% in <i>text</i> to represent ip and password.

* `start_pm_msg (text)` - set private message on a pickup start. Use %pickup_name%, %ip%, %password% and %channel% to represent its values.

* `submsg (text)` - set message on !sub command. Use %pickup_name%, %ip%, %password% and %promotion_role% to represent its values.

* `promotemsg (text)` - set message on !promote command. Use %promotion_role%, %pickup_name% and %required_players% to represent its values.

* `ip (text)` - set ip wich will be shown in startmsg, submsg and on !ip command.

* `password (텍스트)` - set password wich will be shown in startmsg, submsg and on !ip command.

* `maps (맵이름))[, 맵이름...]` - 맵을 설정합니다. 명령어 예시: !set_default maps Sandstorm, Undergrowth, Site

* `pick_captains (0, 1, 2 또는 3)` - 봇이 캡틴을 선택하는 방식을 설정합니다.
  * 변수 ranked의 값이 0인 경우:
    * 0 - 캡틴을 선택하지 않습니다.
    * 1 - 캡틴을 무작위로 선택하되 captains_role을 가지고 있는 플레이어를 우선으로 합니다.
    * 2 - 캡틴을 무작위로 선택합니다.
  * 변수 ranked의 값이 1인 경우:
    * 0 - 캡틴을 선택하지 않습니다.
    * 1 - 캡틴을 captains_role을 가지고 있는 플레이어 중 ELO가 가장 높은 두 플레이어로 선택합니다.
    * 2 - 캡틴을 ELO가 비슷한 무작위 두 플레이어로 선택합니다.
    * 3 - 캡틴을 무작위로 선택합니다.

* `pick_teams (값)` -  팀 선택 방식을 설정합니다. (값)에는 'no_teams', 'auto' 또는 'manual' 중 하나가 들어가야 합니다.
  * no_teams - 봇은 플레이어 목록과 필요하다면 캡틴만을 선정합니다.
  * auto - 봇은 변수 ranked의 값이 0인 경우 랜덤으로 팀을 선정하고 변수 ranked의 값이 1인 경우 ELO에 따라서 팀을 선정합니다.
  * manual - 플레이어가 직접 팀을 선정합니다. (!pick 명령어를 통해서)

* `team_emojis (emoji) (emoji)` - 커스텀 팀 이모지를 설정합니다.

* `team_names (alpha_name) (beta_name)` - 커스텀 팀 이름을 설정합니다. alpha/beta를 사용하는 명령어도 이 설정에 맞게 변경됩니다.

* `pick_order (순서)` - 팀 선정 방식이 수동인 경우 설정된 (순서)대로 팀을 선택하게 합니다. (순서) 예시: 'abababba'. *(team_names의 영향을 받지 않습니다.)*

* `promotion_role (역할이름)` - 프로모션 역할을 (역할이름)으로 설정합니다.

* `promotion_delay (시간)` - !promote 명령어의 쿨타임을 (시간)으로 설정합니다.

* `blacklist_role (역할이름)` - 블랙리스트 역할을 (역할이름)으로 설정합니다. (이 역할을 가진 플레이어를 특정 픽업 대기열에 참여하지 못하게 합니다.)

* `whitelist_role (역할이름)` - 화이트리스트 역할을 (역할이름)으로 설정합니다. (이 역할을 가진 플레이어만 특정 픽업 대기열에 참여할 수 있게 합니다.)

* `require_ready ('none' 또는 시간)` - 픽업 대기열이 다 차면 (시간)내로 플레이어들이 잠수인지 확인하기 위해 준비 상태를 해야 합니다. ('none')인 경우 준비 없이 매칭이 시작됩니다.

* `ranked (0 or 1)` - 등급 시스템을 설정하여 플레이어가 자신의 경기를 보고하도록 한다.

* `ranked_calibrate (0 or 1)` - set to enable rating boost on first 10 user's matches, default on. Only for 'set_default'.

* `ranked_multiplayer (8 ~ 256)` - change rating K-factor (gain/loss multiplyer), default 32. Only for 'set_default'.

* `ranked_streaks (0 or 1)` - set to enable ranked streaks (starting from x1.5 for (3 wins/loses in a row) to x3.0 (6+ wins/loses in a row))

* `initial_rating (값)` - set starting rating for new players.

* `global_expire (시간 또는 'afk' 또는 'none')` - set default_expire value for users without personal settings.

* `match_livetime (시간)` - 경기의 제한시간을 (시간)으로 설정합니다. (시간)이 지나도 경기 결과가 보고되지 않은 경우 자동으로 취소됩니다.


---


*(일부 명령어는 악용될 수 있거나 큰 문제를 일으키기 때문에 숨겨지거나 변경되었습니다.)*
